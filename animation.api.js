/**
 * @author tla
 * @see {@link https://framagit.org/tla}
 *
 * css3-animation
 * @see {@link https://framagit.org/tla/css3-animation}
 *
 * @license MIT
 * @see {@link https://framagit.org/tla/css3-animation/blob/master/LICENSE}
 *
 * @use js-tools-kit
 * @see {@link https://framagit.org/tla/js-tools-kit}
 *
 * animation with css3 in vanillaJS
 *
 * requirement : chrome 4.0+ , IE 10+ , firefox 5.0+ , safari 4.0+ , opera 12.1+
 * */
( function () {
    
    'use strict';
    
    var global ,
        hasProperty = Object.prototype.hasOwnProperty;
    
    try {
        global = Function( 'return this' )() || ( 42, eval )( 'this' );
    } catch ( e ) {
        global = window;
    }
    
    /**
     * js-tools-kit
     * @license MIT
     * @see {@link https://framagit.org/tla/js-tools-kit}
     * */
    ( function () {
        
        function _defineProperty ( obj , key , prop ) {
            Object.defineProperty( obj , key , {
                configurable : false ,
                enumerable : true ,
                writable : false ,
                value : prop
            } );
        }
        
        /**
         * @readonly
         * @property {Window} global
         * */
        _defineProperty( global , 'global' , global );
        
        /**
         * @readonly
         * @function _defineProperty
         * @param {Object} obj
         * @param {string} key
         * @param {*} prop
         * */
        _defineProperty( global , '_defineProperty' , _defineProperty );
        
        /**
         * @readonly
         * @function isset
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isset' , function ( _ ) {
            return _ !== undefined;
        } );
        
        /**
         * @readonly
         * @function isnull
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnull' , function ( _ ) {
            return _ === null;
        } );
        
        /**
         * @readonly
         * @function exist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'exist' , function ( _ ) {
            return isset( _ ) && !isnull( _ );
        } );
        
        /**
         * @readonly
         * @function isdate
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdate' , function ( _ ) {
            return exist( _ ) && _ instanceof Date;
        } );
        
        /**
         * @readonly
         * @function isarray
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isarray' , function ( _ ) {
            return exist( _ ) && Array.isArray( _ );
        } );
        
        /**
         * @readonly
         * @function isobject
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isobject' , function ( _ ) {
            return exist( _ ) &&
                _ instanceof Object && _.toString() === '[object Object]' &&
                _.constructor.hasOwnProperty( 'defineProperty' );
        } );
        
        /**
         * @readonly
         * @function isstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isstring' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'string' || _ instanceof global.String );
        } );
        
        /**
         * @readonly
         * @function isfillstring
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfillstring' , function ( _ ) {
            return isstring( _ ) && !!_.trim();
        } );
        
        /**
         * @readonly
         * @function isboolean
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isboolean' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'boolean' || _ instanceof global.Boolean );
        } );
        
        /**
         * @readonly
         * @function isinteger
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isinteger' , function ( _ ) {
            return exist( _ ) && Number.isInteger( _ );
        } );
        
        /**
         * @readonly
         * @function isfloat
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfloat' , function ( _ ) {
            return exist( _ ) && typeof _ == 'number' && isFinite( _ );
        } );
        
        /**
         * @readonly
         * @function isfunction
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfunction' , function ( _ ) {
            return exist( _ ) && ( typeof _ === 'function' || _ instanceof global.Function );
        } );
        
        /**
         * @readonly
         * @function isevent
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isevent' , function ( _ ) {
            if ( exist( _ ) ) {
                return ( _ instanceof global.Event ) ||
                    ( exist( _.defaultEvent ) && _.defaultEvent instanceof global.Event ) ||
                    ( exist( _.originalEvent ) && _.originalEvent instanceof global.Event );
            }
            return false;
        } );
        
        /**
         * @readonly
         * @function isregexp
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isregexp' , function ( _ ) {
            return exist( _ ) && ( _ instanceof global.RegExp );
        } );
        
        /**
         * @readonly
         * @function isnodelist
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnodelist' , function ( _ ) {
            return exist( _ ) && (
                ( _ instanceof global.NodeList || _ instanceof global.HTMLCollection ) ||
                ( !isset( _.slice ) && isset( _.length ) && typeof _ === 'object' )
            );
        } );
        
        /**
         * @readonly
         * @function isdocument
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isdocument' , function ( _ ) {
            return exist( _ ) && exist( _.defaultView ) && _ instanceof _.defaultView.HTMLDocument;
        } );
        
        /**
         * @readonly
         * @function iswindow
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'iswindow' , function ( _ ) {
            return exist( _ ) && ( _ === global || ( exist( _.window ) &&
                exist( _.window.constructor ) && _ instanceof _.window.constructor ) );
        } );
        
        /**
         * @readonly
         * @function getdocument
         * @param {*} _
         * @return {?HTMLDocument}
         * */
        _defineProperty( global , 'getdocument' , function ( _ ) {
            var tmp;
            
            if ( exist( _ ) ) {
                
                if ( isdocument( _ ) ) {
                    return _;
                }
                
                if ( isdocument( tmp = _.ownerDocument ) ) {
                    return tmp;
                }
                
                if ( iswindow( _ ) ) {
                    return _.document;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function getwindow
         * @param {*} _
         * @return {!Window}
         * */
        _defineProperty( global , 'getwindow' , function ( _ ) {
            var tmp;
            
            if ( iswindow( _ ) ) {
                return _;
            }
            
            if ( tmp = getdocument( _ ) ) {
                return tmp.defaultView;
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function isnode
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isnode' , function ( _ ) {
            var doc , tag;
            
            if ( ( doc = getdocument( _ ) ) && isstring( tag = _.tagName ) ) {
                return ( doc.createElement( tag ) instanceof _.constructor ||
                    getdocument( global ).createElement( tag ) instanceof _.constructor );
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function isfragment
         * @param {*} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isfragment' , function ( _ ) {
            var win;
            
            return ( win = getwindow( _ ) ) &&
                ( _ instanceof win.DocumentFragment || _ instanceof global.DocumentFragment );
        } );
        
        /**
         * @readonly
         * @property {boolean} eventPassiveSupport
         * */
        ( function () {
            var tmp ,
                sup = false ,
                document = getdocument( global );
            
            if ( document ) {
                tmp = document.createDocumentFragment();
                
                var empty = function () {} , setting = {
                    get passive () {
                        sup = true;
                        return false;
                    }
                };
                
                tmp.addEventListener( 'click' , empty , setting );
            }
            
            _defineProperty( global , 'eventPassiveSupport' , sup );
        } )();
        
        /**
         * @readonly
         * @function domLoad
         * @param {function} callback
         * */
        _defineProperty( global , 'domLoad' , ( function () {
            var ready = false ,
                stackfunction = [] ,
                document = getdocument( global );
            
            function stack ( callback ) {
                if ( isfunction( callback ) ) {
                    if ( ready ) {
                        return callback.call( document );
                    }
                    
                    stackfunction.push( callback );
                }
            }
            
            function load () {
                if ( !ready ) {
                    ready = true;
                    
                    stackfunction.forEach( function ( callback ) {
                        callback.call( document );
                    } );
                    
                    stackfunction = null;
                    
                    if ( document ) {
                        document.removeEventListener( 'DOMContentLoaded' , load , true );
                        document.removeEventListener( 'DomContentLoaded' , load , true );
                        document.removeEventListener( 'load' , load , true );
                    }
                    
                    global.removeEventListener( 'load' , load , true );
                }
            }
            
            if ( document ) {
                document.addEventListener( 'DOMContentLoaded' , load , true );
                document.addEventListener( 'DomContentLoaded' , load , true );
                document.addEventListener( 'load' , load , true );
            }
            
            global.addEventListener( 'load' , load , true );
            
            return stack;
        } )() );
        
        /**
         * @readonly
         * @function matchesSelector
         * @param {HTMLElement|Element|Node} e - element
         * @param {string} s - css selector
         * @return {!boolean}
         * */
        _defineProperty( global , 'matchesSelector' , ( function () {
            var document = getdocument( global ) ,
                match = null ,
                tmp;
            
            if ( document ) {
                tmp = document.createElement( 'div' );
                
                match = tmp[ 'matches' ] && 'matches' ||
                    tmp[ 'matchesSelector' ] && 'matchesSelector' ||
                    tmp[ 'webkitMatchesSelector' ] && 'webkitMatchesSelector' ||
                    tmp[ 'mozMatchesSelector' ] && 'mozMatchesSelector' ||
                    tmp[ 'oMatchesSelector' ] && 'oMatchesSelector' ||
                    tmp[ 'msMatchesSelector' ] && 'msMatchesSelector' || null;
            }
            
            return function ( e , s ) {
                if ( !match || !e[ match ] ) {
                    return false;
                }
                
                return e[ match ]( s );
            };
        } )() );
        
        /**
         * @readonly
         * @function nl2br
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'nl2br' , function ( str ) {
            if ( isstring( str ) ) {
                return str
                    .replace( /\n/g , "<br/>" )
                    .replace( /\t/g , "&nbsp;&nbsp;&nbsp;&nbsp;" );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function inarray
         * @param {*} _in
         * @param {Array} array
         * @return {!boolean}
         * */
        _defineProperty( global , 'inarray' , function ( _in , array ) {
            return array.indexOf( _in ) >= 0;
        } );
        
        /**
         * @readonly
         * @function arrayunique
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'arrayunique' , function ( _ ) {
            for ( var n = [] , i = 0 , l = _.length ; i < l ; i++ ) {
                if ( !inarray( _[ i ] , n ) ) {
                    n.push( _[ i ] );
                }
            }
            return n;
        } );
        
        /**
         * @readonly
         * @function typedtoarray
         * @param {*} arg
         * @return {!Array}
         * */
        _defineProperty( global , 'typedtoarray' , function ( arg ) {
            return Array.apply( null , arg );
        } );
        
        /**
         * @readonly
         * @function fragmenttoarray
         * @param {DocumentFragment} frag
         * @return {!Array}
         * */
        _defineProperty( global , 'fragmenttoarray' , function ( frag ) {
            return typedtoarray( frag.children );
        } );
        
        /**
         * @readonly
         * @function getallchildren
         * @param {HTMLElement|Element|Node} element
         * @return {!NodeList}
         * */
        _defineProperty( global , 'getallchildren' , function ( element ) {
            return element.getElementsByTagName( '*' );
        } );
        
        /**
         * @readonly
         * @function emptyNode
         * @param {HTMLElement|Element|Node} element
         * @return {!(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'emptyNode' , function ( element ) {
            var c;
            
            while ( c = element.firstChild ) {
                element.removeChild( c );
            }
            
            return element;
        } );
        
        /**
         * @readonly
         * @function closest
         * @param {HTMLElement|Element|Node} element
         * @param {string} selector
         * @return {?(HTMLElement|Element|Node)}
         * */
        _defineProperty( global , 'closest' , ( function () {
            var document = getdocument( global );
            
            if ( document && isfunction( document.createElement( 'div' ).closest ) ) {
                return function ( element , selector ) {
                    if ( !isnode( element ) ) {
                        return null;
                    }
                    
                    return element.closest( selector );
                };
            }
            
            return function ( element , selector ) {
                if ( !isnode( element ) ) {
                    return null;
                }
                
                if ( matchesSelector( element , selector ) ) {
                    return element;
                }
                
                while ( isnode( element = element.parentNode ) ) {
                    if ( matchesSelector( element , selector ) ) {
                        return element;
                    }
                }
                
                return null;
            };
        } )() );
        
        /**
         * @readonly
         * @function randomstring
         * @return {!string}
         * */
        _defineProperty( global , 'randomstring' , function () {
            return '_' + Math.random().toString( 30 ).substring( 2 );
        } );
        
        /**
         * @readonly
         * @function between
         * @param {number} min
         * @param {number} max
         * @return {!number}
         * */
        _defineProperty( global , 'between' , function ( min , max ) {
            return Math.floor( Math.random() * ( max - min + 1 ) + min );
        } );
        
        /**
         * @readonly
         * @function round
         * @param {number} int
         * @param {number} after
         * @return {!number}
         * */
        _defineProperty( global , 'round' , function ( int , after ) {
            return parseFloat( int.toFixed( isinteger( after ) ? after : int.toString().length ) );
        } );
        
        /**
         * @readonly
         * @function wrapint
         * @param {number} int
         * @param {number} howmany
         * @return {!number}
         * */
        _defineProperty( global , 'wrapinteger' , function ( int , howmany ) {
            int = int.toString();
            
            while ( int.length < howmany ) {
                int = '0' + int;
            }
            
            return int;
        } );
        
        /**
         * @readonly
         * @function jsonparse
         * @param {string} str
         * @return {?Object}
         * */
        _defineProperty( global , 'jsonparse' , function ( str ) {
            var result = null ,
                masterKey;
            
            try {
                result = JSON.parse( str );
            } catch ( e ) {
                try {
                    masterKey = randomstring();
                    
                    global[ masterKey ] = null;
                    
                    eval( 'global[ masterKey ] = ' + str );
                    
                    if ( isobject( global[ masterKey ] ) ) {
                        result = global[ masterKey ] || null;
                    }
                } catch ( e ) {
                    result = null;
                } finally {
                    delete global[ masterKey ];
                }
            }
            
            return result;
        } );
        
        if ( !isfunction( global.setImmediate ) ) {
            ( function () {
                var message = 0 ,
                    register = {} ,
                    prefix = randomstring() + '.immediate';
                
                function getLocation ( location ) {
                    try {
                        if ( isfillstring( location.origin ) && location.origin !== 'null' ) {
                            return location.origin;
                        }
                        
                        if ( location.ancestorOrigins && location.ancestorOrigins.length ) {
                            return location.ancestorOrigins[ 0 ];
                        }
                    } catch ( _ ) {
                        return null;
                    }
                }
                
                var origin = ( function () {
                    var init = global;
                    
                    while ( true ) {
                        var tmp = getLocation( init.location );
                        
                        if ( tmp ) {
                            return tmp;
                        }
                        
                        if ( iswindow( init.parent ) ) {
                            init = init.parent;
                            continue;
                        }
                        
                        return '';
                    }
                } )();
                
                try {
                    global.postMessage( prefix + 1 , function () {} );
                } catch ( e ) {
                    return ( function () {
                        _defineProperty( global , 'setImmediate' , global.setTimeout );
                        _defineProperty( global , 'clearImmediate' , global.clearTimeout );
                    } )();
                }
                
                global.addEventListener( 'message' , function ( event ) {
                    if ( origin === event.origin ) {
                        var i = event.data;
                        
                        if ( isfunction( register[ i ] ) ) {
                            register[ i ]();
                            delete register[ i ];
                            event.stopImmediatePropagation();
                        }
                    }
                } , eventPassiveSupport ? { capture : true , passive : true } : true );
                
                /**
                 * @readonly
                 * @function setImmediate
                 * @param {function} fn
                 * @return {!number}
                 * */
                _defineProperty( global , 'setImmediate' , function ( fn ) {
                    var id = ++message;
                    
                    register[ prefix + id ] = fn;
                    global.postMessage( prefix + id , origin );
                    
                    return id;
                } );
                
                /**
                 * @readonly
                 * @function clearImmediate
                 * @param {number} id
                 * @return void
                 * */
                _defineProperty( global , 'clearImmediate' , function ( id ) {
                    if ( register[ prefix + id ] ) {
                        delete register[ prefix + id ];
                    }
                } );
            } )();
        }
        
        /**
         * @readonly
         * @function getstyleproperty
         * @param {HTMLElement|Element|Node} element
         * @param {string} property
         * @return {?string}
         * */
        _defineProperty( global , 'getstyleproperty' , function ( element , property ) {
            var val , win;
            
            if ( isnode( element ) ) {
                
                if ( val = element.style[ property ] ) {
                    return val;
                }
                
                if ( val = element.style.getPropertyValue( property ) ) {
                    return val;
                }
                
                if ( win = getwindow( element ) ) {
                    return win.getComputedStyle( element ).getPropertyValue( property ) || null;
                }
                
            }
            
            return null;
        } );
        
        /**
         * @readonly
         * @function splitTrim
         * @param {string} str
         * @param {RegExp|string} spl
         * @return {Array}
         * */
        _defineProperty( global , 'splitTrim' , function ( str , spl ) {
            var ar = str.split( spl );
            
            for ( var i = 0 , r = [] , tmp ; i < ar.length ; ) {
                if ( tmp = ar[ i++ ].trim() ) {
                    r.push( tmp );
                }
            }
            
            return r;
        } );
        
        /**
         * @readonly
         * @function clonearray
         * @param {Array} _
         * @return {!Array}
         * */
        _defineProperty( global , 'clonearray' , function ( _ ) {
            return _.slice( 0 );
        } );
        
        /**
         * @readonly
         * @function toarray
         * @param {*} _
         * @return {!Array}
         * */
        _defineProperty( global , 'toarray' , function ( _ ) {
            return isset( _ ) ? ( isarray( _ ) ? _ : [ _ ] ) : [];
        } );
        
        /**
         * @readonly
         * @function counterCallback
         * @param {number} counter
         * @param {function} callback
         * @return {!Array}
         * */
        _defineProperty( global , 'counterCallback' , function ( counter , callback ) {
            var current = 0;
            
            return function () {
                if ( ++current == counter ) {
                    callback();
                }
            };
        } );
        
        /**
         * @readonly
         * @function regexpEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'regexpEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /\\/gi , '\\\\' )
                    .replace( /([.\[\](){}^?*|+\-$])/gi , '\\$1' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function htmlEscapeString
         * @param {string} str
         * @return {!string}
         * */
        _defineProperty( global , 'htmlEscapeString' , function ( str ) {
            if ( exist( str ) ) {
                return str.toString()
                    .replace( /&/gi , '&amp;' )
                    .replace( /"/gi , '&quot;' )
                    .replace( /'/gi , '&apos;' )
                    .replace( /</gi , '&lt;' )
                    .replace( />/gi , '&gt;' );
            }
            
            return '';
        } );
        
        /**
         * @readonly
         * @function resetregexp
         * @param {RegExp} _
         * @return {?RegExp}
         * */
        _defineProperty( global , 'resetregexp' , function ( _ ) {
            if ( !isregexp( _ ) ) {
                return null;
            }
            
            return _.lastIndex = _.index = 0, _;
        } );
        
        /**
         * @readonly
         * @function overrideObject
         * @return {!Object}
         * */
        _defineProperty( global , 'overrideObject' , function () {
            var result = {} ,
                array = typedtoarray( arguments );
            
            if ( array.length <= 0 ) {
                return result;
            }
            
            if ( array.length == 1 ) {
                return isobject( array[ 0 ] ) ? array[ 0 ] : result;
            }
            
            for ( var i = 0 , l = array.length ; i < l ; i++ ) {
                forin( array[ i ] , function ( key , value ) {
                    result[ key ] = value;
                } );
            }
            
            return result;
        } );
        
        /**
         * @readonly
         * @function isEmptyObject
         * @param {Object} _
         * @return {!boolean}
         * */
        _defineProperty( global , 'isEmptyObject' , function ( _ ) {
            if ( isobject( _ ) ) {
                for ( var key in _ ) {
                    if ( hasProperty.call( _ , key ) ) {
                        return false;
                    }
                }
                
                return true;
            }
            
            return false;
        } );
        
        /**
         * @readonly
         * @function forin
         * @param {Object} obj
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'forin' , function ( obj , callback ) {
            var key , value;
            
            if ( isobject( obj ) ) {
                for ( key in obj ) {
                    if ( hasProperty.call( obj , key ) && isset( value = obj[ key ] ) ) {
                        callback( key , value , obj );
                    }
                }
            }
        } );
        
        /**
         * @readonly
         * @function foreach
         * @param {Array} arr
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'foreach' , function ( arr , callback ) {
            if ( isarray( arr ) ) {
                for ( var i = 0 , l = arr.length ; i < l ; i++ ) {
                    callback( arr[ i ] , i , arr );
                }
            }
        } );
        
        /**
         * @readonly
         * @function for
         * @param {Array} arg
         * @param {function} callback
         * @return void
         * */
        _defineProperty( global , 'for' , function ( arg , callback ) {
            if ( isarray( arg ) ) {
                foreach( arg , callback );
            }
            else if ( isobject( arg ) ) {
                forin( arg , callback );
            }
        } );
        
        /**
         * @readonly
         * @function smoothyIncrement
         * @param {Object} setting
         * @param {number} setting.begin
         * @param {number} setting.end
         * @param {function} setting.eachFrame
         * @param {function} [setting.onFinish]
         * @param {number} [setting.speed=500]
         * @return void
         * */
        _defineProperty( global , 'smoothyIncrement' , ( function () {
            
            var frameLatency = 1000 / 60;
            
            var requestFrame = global[ 'requestAnimationFrame' ] ||
                global[ 'webkitRequestAnimationFrame' ] ||
                global[ 'mozRequestAnimationFrame' ] ||
                global[ 'msRequestAnimationFrame' ];
            
            var cancelFrame = global[ 'cancelAnimationFrame' ] ||
                global[ 'webkitCancelAnimationFrame' ] ||
                global[ 'mozCancelAnimationFrame' ] ||
                global[ 'msCancelAnimationFrame' ];
            
            function ease ( n ) {
                return 0.5 * ( 1 - Math.cos( Math.PI * n ) );
            }
            
            function animate ( callframe ) {
                var frame , start;
                
                function loop () {
                    frame = requestFrame( loop );
                    callframe();
                }
                
                start = setImmediate( loop );
                
                return function () {
                    clearImmediate( start );
                    cancelFrame( frame );
                };
            }
            
            return function ( setting ) {
                
                setting = overrideObject( {
                    speed : 1000
                } , setting );
                
                var begin = setting.begin ,
                    end = setting.end ,
                    speed = setting.speed ,
                    callback = setting.eachFrame ,
                    callbackFinish = setting.onFinish;
                
                if ( !isinteger( speed ) || speed <= 0 ) {
                    speed = 1000;
                }
                
                speed = Math.round( speed / 1.8 );
                
                var tmp = begin;
                begin = Math.min( begin , end );
                end = Math.max( tmp , end );
                
                var newval , elapsed;
                
                var firstStep = true ,
                    lastStep = false ,
                    endAtNext = false;
                
                var beginValues = [] ,
                    beginTotal = 0 ,
                    total = 0;
                
                var elapsedMax = 1 ,
                    elapsedMin = 0.1;
                
                var totalScroll = end - begin ,
                    increment = Math.round( totalScroll / ( speed / frameLatency ) );
                
                if ( totalScroll <= increment ) {
                    return callback( end , end - begin );
                }
                
                var startTime = Date.now();
                
                var stop = animate( function () {
                    
                    /* stop process */
                    /* ---------------------- */
                    if ( endAtNext ) {
                        callback( end , end - begin );
                        
                        if ( isfunction( callbackFinish ) ) {
                            callbackFinish();
                        }
                        
                        return stop();
                    }
                    
                    /* calc process */
                    /* ---------------------- */
                    
                    elapsed = round( ( Date.now() - startTime ) / speed , 2 );
                    
                    if ( elapsed < elapsedMin ) {
                        elapsed = elapsedMin;
                    }
                    else if ( elapsed > elapsedMax ) {
                        elapsed = elapsedMax;
                    }
                    
                    if ( firstStep && elapsed == elapsedMax ) {
                        firstStep = false;
                    }
                    
                    /* begin process */
                    /* ---------------------- */
                    
                    newval = Math.round( increment * ease( elapsed ) );
                    
                    if ( newval < 1 ) {
                        newval = 1;
                    }
                    
                    /* middle process */
                    /* ---------------------- */
                    
                    if ( firstStep ) {
                        beginValues.push( newval );
                        beginTotal += newval;
                    }
                    
                    if ( !firstStep && !lastStep && ( totalScroll - total ) <= beginTotal ) {
                        lastStep = true;
                    }
                    
                    /* end process */
                    /* ---------------------- */
                    
                    if ( lastStep ) {
                        if ( beginValues.length ) {
                            newval = beginValues.pop();
                        }
                        else {
                            newval = 1;
                        }
                    }
                    
                    total += newval;
                    
                    /* request stop process */
                    /* ---------------------- */
                    
                    if ( lastStep && totalScroll - total <= newval ) {
                        endAtNext = true;
                    }
                    
                    /* callback */
                    /* ---------------------- */
                    
                    callback( begin += newval , newval );
                    
                } );
                
                return stop;
                
            };
            
        } )() );
        
        /**
         * @readonly
         * @function cacheHandler
         * @param {number} [n=500]
         * @return {!function}
         * */
        _defineProperty( global , 'cacheHandler' , function ( n ) {
            var c = [];
            !n && ( n = 500 );
            
            function SAVE ( k , v ) {
                if ( !/^_(rm|purge)$/g.test( k ) ) {
                    SAVE[ k ] = v;
                    c.push( k );
                    
                    if ( c.length > n ) {
                        delete SAVE[ c.shift() ];
                    }
                }
            }
            
            SAVE._rm = function ( k ) {
                var i;
                
                if ( ( i = c.indexOf( k ), i ) !== -1 ) {
                    delete SAVE[ k ];
                    c.splice( i , 1 );
                }
            };
            
            SAVE._purge = function () {
                var i = c.length;
                
                while ( i-- ) {
                    delete SAVE[ c.shift() ];
                }
            };
            
            return SAVE;
        } );
        
        /**
         * @readonly
         * @function requireJS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireJS' , ( function () {
            var doc = getdocument( global );
            
            function require ( src , callback , document ) {
                var script;
                
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && script.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'script[src="' + src + '"]' ) ) {
                        return load();
                    }
                    
                    script = document.createElement( 'script' );
                    
                    script.setAttribute( 'src' , src );
                    script.setAttribute( 'async' , 'async' );
                    script.setAttribute( 'type' , 'text/javascript' );
                    
                    script.addEventListener( 'load' , load );
                    
                    document.head.appendChild( script );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function requireCSS
         * @param {string|Array} source
         * @param {function} callback
         * @param {Document} [document=Document]
         * @return void
         * */
        _defineProperty( global , 'requireCSS' , ( function () {
            var doc = getdocument( global );
            
            function require ( href , callback , document ) {
                var link;
                
                function load ( e ) {
                    callback();
                    e && e.type == 'load' && link.removeEventListener( 'load' , load );
                }
                
                if ( document ) {
                    
                    if ( document.querySelector( 'link[href="' + href + '"]' ) ) {
                        return load();
                    }
                    
                    link = document.createElement( 'link' );
                    
                    link.setAttribute( 'href' , href );
                    link.setAttribute( 'rel' , 'stylesheet' );
                    
                    link.addEventListener( 'load' , load );
                    
                    document.head.appendChild( link );
                    
                }
            }
            
            return function ( source , callback , document ) {
                var scripts = toarray( source ) ,
                    l = scripts.length ,
                    i = 0;
                
                function loop () {
                    if ( i < l ) {
                        return require( scripts[ i++ ] , loop , document || doc );
                    }
                    
                    if ( isfunction( callback ) ) {
                        callback();
                    }
                }
                
                loop();
            };
            
        } )() );
        
        /**
         * @readonly
         * @function htmlDOM
         * @param {string} str
         * @return {?(HTMLElement|Element|Node|array)}
         * */
        _defineProperty( global , 'htmlDOM' , ( function () {
            var document = getdocument( global );
            
            if ( !document ) {
                return function () {
                    return null;
                };
            }
            
            var parentOf = {
                "col" : "colgroup" ,
                "tr" : "tbody" ,
                "th" : "tr" ,
                "td" : "tr" ,
                "colgroup" : "table" ,
                "tbody" : "table" ,
                "thead" : "table" ,
                "tfoot" : "table" ,
                "dt" : "dl" ,
                "dd" : "dl" ,
                "figcaption" : "figure" ,
                "legend" : "fieldset" ,
                "fieldset" : "form" ,
                "keygen" : "form" ,
                "area" : "map" ,
                "menuitem" : "menu" ,
                "li" : "ul" ,
                "option" : "optgroup" ,
                "optgroup" : "select" ,
                "output" : "form" ,
                "rt" : "ruby" ,
                "rp" : "ruby" ,
                "summary" : "details" ,
                "track" : "video" ,
                "source" : "video" ,
                "param" : "object"
            };
            
            var _ = cacheHandler();
            
            function clone ( e ) {
                if ( isarray( e ) ) {
                    for ( var i = 0 , l = e.length , r = [] ; i < l ; i++ ) {
                        r.push( e[ i ].cloneNode( true ) );
                    }
                    return r;
                }
                
                if ( exist( e ) ) {
                    return e.cloneNode( true );
                }
                
                return null;
            }
            
            function child ( e ) {
                return e.children.length == 1 ? e.children[ 0 ] : Array.apply( null , e.children );
            }
            
            function createHTML ( str ) {
                var firstTag = ( /^(<[\s]*[\w]{1,15}[\s]*(\b|>)?)/gi.exec( str ) || [ '' ] )[ 0 ]
                    .replace( /\W/g , '' ).trim().toLowerCase() ,
                    parent , doc;
                
                if ( !firstTag ) {
                    return;
                }
                
                if ( !parentOf[ firstTag ] ) {
                    doc = document.createDocumentFragment();
                    parent = document.createElement( 'DIV' );
                    doc.appendChild( parent );
                    parent.innerHTML = str;
                    return child( parent );
                }
                
                /** @type {Node} */
                parent = createHTML( '<' + parentOf[ firstTag ] + '></' + parentOf[ firstTag ] + '>' );
                
                while ( parent.firstChild ) {
                    parent = parent.firstChild;
                }
                
                parent.innerHTML = str;
                
                return child( parent );
            }
            
            return function ( str ) {
                var r;
                
                str = str.trim();
                
                if ( _[ str ] ) {
                    return clone( _[ str ] );
                }
                
                if ( !/(<[\s]*(\/)?[\s]*[\w]{1,15}[\s]*(\/)?[\s]*(\b|>)?)/gi.test( str ) ) {
                    return null;
                }
                
                if ( /^(<\/[\s]*[\w]{1,15}[\s]*>)$/gi.test( str ) ) {
                    r = document.createElement( str.replace( /\W/g , '' ).toUpperCase() );
                }
                else {
                    r = createHTML( str );
                }
                
                return _( str , r ) , clone( r );
            };
        } )() );
        
    } )();
    
    var fingerprintCache = cacheHandler() ,
        
        guid = 'guid-animation-' + randomstring() ,
        
        patternTime = {
            slow : 850 ,
            fast : 250 ,
            origin : 400
        } ,
        
        // save display property
        sdp = ( function () {
            var data = {};
            
            return {
                
                get : function ( element ) {
                    return data[ element[ guid ] ] || null;
                } ,
                
                set : function ( element ) {
                    data[ element[ guid ] ] = element.style.getPropertyValue( 'display' );
                }
                
            };
        } )() ,
        
        stylesSnapshot = ( function () {
            var cssOrigin = {} ,
                cssToRemove = {};
            
            return {
                
                make : function ( element , properties ) {
                    var i = 0 ,
                        origin = {} ,
                        toRemove = [] ,
                        l = properties.length ,
                        id = element[ guid ] ,
                        manual = parseCss( element.style.cssText );
                    
                    for ( ; i < l ; i++ ) {
                        if ( !!manual[ properties[ i ] ] ) {
                            origin[ properties[ i ] ] = manual[ properties[ i ] ];
                        }
                        else {
                            toRemove.push( properties[ i ] );
                        }
                    }
                    
                    cssOrigin[ id ] = cssOrigin[ id ] ? overrideObject( cssOrigin[ id ] , origin ) : origin;
                    cssToRemove[ id ] = cssToRemove[ id ] ? overrideObject( cssToRemove[ id ] , toRemove ) : toRemove;
                } ,
                
                recover : function ( element ) {
                    var i = 0 ,
                        id = element[ guid ] ,
                        origin = cssOrigin[ id ] ,
                        toRemove = cssToRemove[ id ] ,
                        l = toRemove.length;
                    
                    for ( ; i < l ; i++ ) {
                        element.style.removeProperty( toRemove[ i ] );
                    }
                    
                    forin( origin , function ( key , value ) {
                        element.style.setProperty( key , value.val , value.imp );
                    } );
                    
                    delete cssOrigin[ id ];
                    delete cssToRemove[ id ];
                } ,
                
                erase : function ( element ) {
                    var id;
                    
                    if ( id = element[ guid ] ) {
                        delete cssOrigin[ id ];
                        delete cssToRemove[ id ];
                    }
                }
                
            };
        } )();
    
    function setCss ( node , json ) {
        forin( json , function ( key , value ) {
            node.style.setProperty( key , value );
        } );
    }
    
    function hackAuto ( v ) {
        return v.trim().toLowerCase() === 'auto' ? null : v;
    }
    
    function fingerprint ( c , m ) {
        var s = JSON.stringify( c ) , r , i , l , t;
        
        if ( t = fingerprintCache[ ' ' + s ] ) {
            return t;
        }
        
        for ( r = 0, i = 0, l = s.length ; i < l ; ) {
            r = ( ( r << 5 ) - r ) + s.charCodeAt( i++ );
            r = r & r;
        }
        
        t = parseInt( ( r + s.length ).toString() + m );
        fingerprintCache( s , ' ' + t );
        
        return t;
    }
    
    function parseCss ( cssText ) {
        var split = cssText.split( ';' ) ,
            l = split.length ,
            css = {} ,
            i = 0 ,
            tmp , value;
        
        for ( ; i < l ; ) {
            if ( tmp = split[ i++ ].trim() ) {
                tmp = tmp.split( ':' ), value = tmp[ 1 ].trim();
                
                css[ tmp[ 0 ].trim() ] = !( tmp = /(!important)$/gi.exec( value ) ) ?
                    {
                        val : value ,
                        imp : ""
                    } :
                    {
                        val : value.substring( 0 , tmp.index ).trim() ,
                        imp : "important"
                    };
            }
        }
        
        return css;
    }
    
    function dataAnim ( c , d ) {
        return [
            isfunction( c ) && c || isfunction( d ) && d || null ,
            ( isset( c ) && isinteger( c ) ) && +c || patternTime[ c ] || patternTime.origin
        ];
    }
    
    function build ( document , global ) {
        
        function is_hidden ( element ) {
            return ( getstyleproperty( element , 'opacity' ) == '0' ||
                getstyleproperty( element , 'display' ) === 'none' ||
                getstyleproperty( element , 'visibility' ) === 'hidden' );
        }
        
        function getEmulateDisplayProp ( tag ) {
            var fragment = document.createDocumentFragment() ,
                node = document.createElement( tag.toUpperCase() ) ,
                c;
            
            fragment.appendChild( node );
            
            c = getstyleproperty( node , 'display' );
            
            if ( c == 'none' ) {
                c = '';
            }
            
            return fragment = node = null, c;
        }
        
        function getVisibleDisplayValue ( node ) {
            var prop ,
                current = save( node , [ 'display' ] ) ,
                initial = function () {
                    if ( current.display.computed != 'none' ) {
                        return '';
                    }
                    
                    return getEmulateDisplayProp( node.tagName ) || 'block';
                };
            
            prop = sdp.get( node );
            
            if ( isstring( prop ) && prop == 'none' ) {
                return initial();
            }
            
            prop = current.display.inline;
            
            if ( !isstring( prop ) || prop == 'none' ) {
                return initial();
            }
            
            return prop;
        }
        
        function calcProp ( element , prop ) {
            var tmp = element.cloneNode( true ) , data , _prop;
            
            _prop = prop.slice( 0 , 1 ).toUpperCase() + prop.slice( 1 ).toLowerCase();
            
            tmp.style.cssText = element.style.cssText;
            
            setCss( tmp , {
                "position" : "absolute" ,
                "visibility" : "hidden" ,
                "display" : "block"
            } );
            
            element.parentNode.appendChild( tmp );
            data = hackAuto( getstyleproperty( tmp , prop ) ) || ( tmp[ 'client' + _prop ] || tmp[ 'offset' + _prop ] ) + 'px';
            
            return tmp.parentNode.removeChild( tmp ), data;
        }
        
        function callEnd ( done , node , p ) {
            if ( done ) {
                if ( Array.isArray( done ) ) {
                    done.forEach( function ( fn ) {
                        fn.call( node , p );
                    } );
                }
                else {
                    done.call( node , p );
                }
            }
        }
        
        function getRightOpacity ( data ) {
            var properties = [ 'inline' , 'computed' ];
            
            for ( var i = 0 , l = properties.length , property ; i < l ; i++ ) {
                property = properties[ i ];
                
                if ( exist( data[ property ] ) && data[ property ] != 0 ) {
                    return data[ property ];
                }
            }
            
            return '1';
        }
        
        function downQueue ( element ) {
            var setting , data = _anime.element[ element[ guid ] ];
            
            // continue animation if need, else state = false
            if ( data[ 'queue' ].length <= 0 ) {
                delete _anime.element[ element[ guid ] ];
                delete element[ guid ];
                data = null;
            }
            else {
                // get arguments of next animation
                setting = data[ 'queue' ][ 0 ];
                
                // remove her of queue
                data[ 'queue' ].splice( 0 , 1 );
                data = null;
                
                // force animation and start
                setting.force = true;
                Animate.prototype.init.call( setting );
                setting = null;
            }
        }
        
        function save ( element , array ) {
            var r = {} ,
                manual = parseCss( element.style.cssText );
            
            array.forEach( function ( _ ) {
                var inline = manual[ _ ];
                
                r[ _ ] = {
                    inline : inline ? inline.val : null ,
                    
                    computed : ( function () {
                        var tmp;
                        
                        !!inline && element.style.removeProperty( _ );
                        
                        tmp = getstyleproperty( element , _ );
                        
                        !!inline && element.style.setProperty( _ , inline.val , inline.imp );
                        
                        return tmp;
                    } )()
                };
            } );
            
            return r;
        }
        
        function checkUselessProp ( element , saved , type ) {
            
            switch ( type ) {
                
                case 'show':
                    ( function () {
                        
                        var opacity = saved[ 'opacity' ];
                        if ( opacity.computed != 0 ||
                            ( opacity.inline != 0 && opacity.computed != 0 ) ) {
                            element.style.removeProperty( 'opacity' );
                        }
                        
                        var visibility = saved[ 'visibility' ];
                        if ( visibility.computed != 'hidden' ||
                            ( visibility.inline != 'hidden' && visibility.computed != 'hidden' ) ) {
                            element.style.removeProperty( 'visibility' );
                        }
                        
                        var display = saved[ 'display' ];
                        if ( display.computed != 'none' ||
                            ( display.inline != 'none' && display.computed != 'none' ) ) {
                            element.style.removeProperty( 'display' );
                        }
                        
                    } )();
                    break;
                
                case 'hide':
                    ( function () {
                        
                        element.style.removeProperty( 'opacity' );
                        element.style.removeProperty( 'visibility' );
                        
                        var display = saved[ 'display' ];
                        if ( display.computed == 'none' ) {
                            element.style.removeProperty( 'display' );
                        }
                        
                    } )();
                    break;
                
            }
            
        }
        
        // animation events start / end
        ( function () {
            var i = 0 ,
                eventEnd = [
                    'animationend' ,
                    'oanimationend' ,
                    'MSAnimationEnd' ,
                    'mozAnimationEnd' ,
                    'webkitAnimationEnd'
                ] ,
                eventStart = [
                    'animationstart' ,
                    'oanimationstart' ,
                    'MSAnimationStart' ,
                    'mozAnimationStart' ,
                    'webkitAnimationStart'
                ];
            
            var capture = function ( cap ) {
                if ( eventPassiveSupport ) {
                    return {
                        capture : !!cap ,
                        passive : true
                    };
                }
                
                return !!cap;
            };
            
            for ( ; i < eventEnd.length ; i++ ) {
                global.addEventListener( eventEnd[ i ] , _anime.animationEnd , capture( true ) );
                global.addEventListener( eventStart[ i ] , _anime.animationStart , capture( true ) );
            }
        } )();
        
        function _anime ( setting ) {
            return new Animate( {
                element : setting.element ,
                
                begin : setting.begin || null ,
                done : setting.done || null ,
                
                how : setting.how || 'linear' ,
                force : !!setting.force ,
                merge : !!setting.merge ,
                wait : !!setting.wait ,
                
                properties : setting.properties || null ,
                duration : setting.duration || null ,
                fingerprint : ( function () {
                    if ( !setting.properties || setting.wait ) {
                        return null;
                    }
                    
                    return fingerprint( setting.properties , setting.duration );
                } )()
            } );
        }
        
        _anime.id = 0;
        _anime.rand = 0;
        _anime.busy = [];
        _anime.cache = {};
        _anime.element = {};
        _anime.prename = '_' + randomstring() + '-';
        
        _anime.prefix = ( function () {
            var test = document.createElement( 'DIV' ).style ,
                
                animation = isset( test[ 'animation' ] ) && '' ||
                    ( isset( test[ 'mozAnimation' ] ) || isset( test[ 'MozAnimation' ] ) ) && '-moz-' || // mozilla
                    ( isset( test[ 'oAnimation' ] ) || isset( test[ 'OAnimation' ] ) ) && '-o-' || // opera
                    isset( test[ 'webkitAnimation' ] ) && '-webkit-' || // webkit
                    '' , // native
                
                transform = ( isset( test[ 'mozTransform' ] ) || isset( test[ 'MozTransform' ] ) ) && '-moz-' || // mozilla
                    ( isset( test[ 'oTransform' ] ) || isset( test[ 'OTransform' ] ) ) && '-o-' || // opera
                    isset( test[ 'webkitTransform' ] ) && '-webkit-' || // webkit
                    isset( test[ 'msTransform' ] ) && '-ms-' || // microsoft
                    ''; // native
            
            return test = null, {
                animation : animation ,
                transform : transform
            };
        } )();
        
        _anime.animationStart = function ( e ) {
            var element = e.target , data;
            
            if ( !!element[ guid ] && !!_anime.element[ element[ guid ] ] ) {
                e.stopPropagation();
                
                data = _anime.element[ element[ guid ] ][ e.animationName ];
                
                clearTimeout( data.timeout );
                
                // forcing end animation ( hidden or disabled element can freeze the animation event )
                data.timeout = setTimeout( function () {
                    _anime.animationEnd( {
                        target : element ,
                        animationName : data.name
                    } , data );
                } , data.ms );
            }
        };
        
        _anime.animationEnd = function ( e , b , p , nocall ) {
            var element = e.target , data;
            
            if ( !!b || ( !!element[ guid ] && !!_anime.element[ element[ guid ] ] ) ) {
                !b && e.stopImmediatePropagation();
                
                if ( data = !!b ? b : _anime.element[ element[ guid ] ][ e.animationName ] ) {
                    clearTimeout( data.timeout );
                    
                    // set final css
                    !p && setCss( element , data.css );
                    
                    // remove keyframes class
                    element.classList.remove( e.animationName );
                    
                    // done callback
                    if ( !nocall && data.done ) {
                        callEnd( data.done , element , p );
                    }
                    
                    delete _anime.element[ element[ guid ] ][ e.animationName ];
                    
                    _anime.busy.splice( _anime.busy.indexOf( e.animationName ) , 1 );
                    
                    // clear cache if it's too big
                    if ( _anime.busy.length <= 0 && Object.keys( _anime.cache ).length > 500 ) {
                        _anime.cache = {};
                        _anime.style.reset();
                    }
                    
                    data = null;
                    !p && downQueue( element , p );
                }
            }
        };
        
        _anime.style = ( function () {
            var style = document.createElement( 'STYLE' ) ,
                head = document.getElementsByTagName( 'HEAD' )[ 0 ] ,
                back;
            
            style.setAttribute( 'type' , 'text/css' );
            head.appendChild( style );
            
            back = ( function ( rule ) {
                return {
                    add : function ( selector , property ) {
                        rule.insertRule( selector + '{' + property + '}' , rule.cssRules.length );
                    } ,
                    reset : function () {
                        while ( rule.cssRules.length ) {
                            rule.deleteRule( rule.cssRules.length - 1 );
                        }
                    }
                };
            } )( style[ 'sheet' ] );
            
            return back;
        } )();
        
        function Animate ( setting ) {
            var merge = setting.merge ,
                nguid , data , i , t;
            
            this.css = setting.properties;
            this.fingerprint = setting.fingerprint;
            this.duration = setting.duration;
            this.wait = setting.wait;
            this.done = setting.done;
            this.force = setting.force;
            this.node = setting.element;
            this.timing = setting.how;
            this.begin = setting.begin;
            
            // node guid
            if ( !( nguid = this.node[ guid ] ) ) {
                this.node[ guid ] = nguid = ++_anime.id;
            }
            
            // animations indexer
            if ( !_anime.element[ nguid ] ) {
                _anime.element[ nguid ] = {};
            }
            
            // indexer data
            this.data = _anime.element[ nguid ];
            
            // merge new animation with current animation
            if ( merge && this.data.current ) {
                this.fingerprint = null;
                
                // before inst
                data = this.data[ this.data.current.name ];
                
                // merge current css
                for ( i in data.css ) {
                    t = getstyleproperty( this.node , i );
                    
                    if ( t == data.css[ i ] ) {
                        delete data.css[ i ];
                        continue;
                    }
                    
                    this.node.style.setProperty( i , t );
                }
                
                // end current animation
                _anime.animationEnd( {
                    target : this.node ,
                    animationName : data.name
                } , data , true , true );
                
                // merge setting
                this.css = overrideObject( data.css , this.css );
                this.data.state = false;
                
                if ( this.done ) {
                    if ( isarray( data.done ) ) {
                        this.done = data.done.concat( this.done );
                    }
                    else {
                        this.done = [ data.done , this.done ];
                    }
                }
                else {
                    this.done = data.done;
                }
            }
            
            this.init();
        }
        
        Animate.prototype = {
            
            gotoNext : function () {
                downQueue( this.node );
            } ,
            
            pattern : function () {
                return {
                    css : this.css ,
                    timeout : null ,
                    done : this.done ,
                    name : this.animationName ,
                    ms : parseInt( this.duration * 1.15 )
                };
            } ,
            
            // wait...
            pause : function () {
                var self = this;
                
                setTimeout( function () {
                    
                    // call and delete callback
                    !!self.data.done && callEnd( self.data.done , self.node );
                    
                    // continue animation if need, else state = false
                    self.gotoNext();
                    
                } , this.duration );
            } ,
            
            // set animation data, before build keyframes
            init : function () {
                // list of animation
                if ( !this.data.queue ) {
                    this.data.queue = [];
                }
                
                // if !force ( by library ) && animation state = true, register list
                if ( !this.force && !!this.data.state ) {
                    return this.data.queue.push( this );
                }
                
                // state = true, animation is begin !
                this.data.state = true;
                
                // just for wait, no animation
                if ( this.wait ) {
                    return this.pause();
                }
                
                // before start callback, can stop animation
                if ( isfunction( this.begin ) && this.begin.call( this.node ) === 'stop' ) {
                    return this.gotoNext();
                }
                
                // create keyframe
                this.genKeyframes();
            } ,
            
            start : function () {
                _anime.busy.push( this.animationName );
                this.data.current = this.data[ this.animationName ] = this.pattern();
                this.defineTimeEnd().node.classList.add( this.animationName );
            } ,
            
            // generate keyframe
            genKeyframes : function () {
                
                // check cache, begin animation
                if ( this.fingerprint && ( this.animationName = _anime.cache[ this.fingerprint ] ) ) {
                    return this.start();
                }
                
                // animation name
                this.animationName = _anime.prename + 'animation_' + _anime.rand++;
                
                // set css property
                var to = '';
                
                forin( this.css , function ( key , value ) {
                    to += key + ':' + value + ';';
                } );
                
                // generate new keyframes
                _anime.style.add(
                    '@' + _anime.prefix.animation + 'keyframes ' + this.animationName ,
                    'to{' + to + '}'
                );
                
                // generate new  css class
                _anime.style.add(
                    '.' + this.animationName ,
                    _anime.prefix.animation + 'animation:' + this.animationName + ' ' +
                    this.duration + 'ms ' + this.timing + ' 0s 1 normal forwards running !important'
                );
                
                // begin animation
                this.start();
                
                // save cache
                if ( this.fingerprint ) {
                    _anime.cache[ this.fingerprint ] = this.animationName;
                }
            } ,
            
            defineTimeEnd : function () {
                var self = this ,
                    data = this.data[ this.animationName ];
                
                // forcing end animation ( hidden or disabled element can freeze the animation event )
                data.timeout = setTimeout( function () {
                    _anime.animationEnd( {
                        target : self.node ,
                        animationName : data.name
                    } , data );
                } , data.ms );
                
                return this;
            }
            
        };
        
        return {
            
            kill : function ( element ) {
                var nguid , data , current;
                
                if ( nguid = element[ guid ] ) {
                    
                    if ( data = _anime.element[ nguid ] ) {
                        
                        if ( current = data.current ) {
                            clearTimeout( current.timeout );
                            element.classList.remove( current.name );
                            _anime.busy.splice( _anime.busy.indexOf( current.name ) , 1 );
                        }
                        
                        delete _anime.element[ nguid ];
                    }
                    
                    stylesSnapshot.erase( element );
                    
                    delete element[ guid ];
                }
            } ,
            
            animate : function ( element , css , ms , opt , call , merge ) {
                // get callback
                var back = isfunction( call ) && call ||
                    isfunction( opt ) && opt ||
                    isfunction( ms ) && ms ||
                    false ,
                    
                    tmp;
                
                merge = isboolean( ms ) ? ms :
                    isboolean( opt ) ? opt :
                        isboolean( call ) ? call :
                            !!merge;
                
                // get css property and timing if string
                if ( isstring( css ) && isstring( ms ) ) {
                    tmp = css, css = {}, css[ tmp ] = ms;
                    ms = isinteger( opt ) && +opt || patternTime.origin;
                }
                else {
                    ms = isinteger( ms ) ? +ms : patternTime.origin;
                }
                
                _anime( {
                    element : element ,
                    properties : css ,
                    duration : ms ,
                    done : back ,
                    merge : merge
                } );
            } ,
            
            wait : function ( element , delay , call ) {
                var data = dataAnim( delay , call );
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    done : data[ 0 ] ,
                    wait : true
                } );
            } ,
            
            fadeIn : function ( element , dur , call ) {
                // get timing and callback
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    properties : {
                        'opacity' : getRightOpacity( saved.opacity )
                    } ,
                    
                    begin : function () {
                        if ( !is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        stylesSnapshot.make( this , [
                            "opacity" ,
                            "visibility"
                        ] );
                        
                        // before animation start , set first property of fade
                        setCss( this , {
                            "opacity" : "0" ,
                            "visibility" : "visible" ,
                            "display" : getVisibleDisplayValue( this )
                        } );
                    } ,
                    
                    done : function () {
                        checkUselessProp( element , saved , 'show' );
                        
                        stylesSnapshot.recover( this );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            fadeOut : function ( element , dur , call ) {
                // get timing and callback
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    properties : {
                        'opacity' : '0'
                    } ,
                    
                    begin : function () {
                        if ( is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        sdp.set( this );
                        
                        stylesSnapshot.make( this , [
                            "opacity" ,
                            "pointer-events"
                        ] );
                        
                        setCss( this , {
                            "pointer-events" : "none"
                        } );
                    } ,
                    
                    done : function () {
                        checkUselessProp( element , saved , 'hide' );
                        
                        this.style.display = 'none';
                        
                        stylesSnapshot.recover( this );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            toggleFade : function ( element , dur , call ) {
                if ( is_hidden( element ) ) {
                    this.fadeIn( element , dur , call );
                }
                else {
                    this.fadeOut( element , dur , call );
                }
            } ,
            
            slideDown : function ( element , dur , call ) {
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    height = calcProp( element , 'height' ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    properties : {
                        "height" : height ,
                        "margin-top" : getstyleproperty( element , "margin-top" ) ,
                        "padding-top" : getstyleproperty( element , "padding-top" ) ,
                        "margin-bottom" : getstyleproperty( element , "margin-bottom" ) ,
                        "padding-bottom" : getstyleproperty( element , "padding-bottom" )
                    } ,
                    
                    begin : function () {
                        if ( !is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        stylesSnapshot.make( this , [
                            "height" ,
                            "overflow" ,
                            "margin-top" ,
                            "padding-top" ,
                            "margin-bottom" ,
                            "padding-bottom"
                        ] );
                        
                        setCss( this , {
                            "height" : "0" ,
                            "margin-top" : "0" ,
                            "padding-top" : "0" ,
                            "overflow" : "hidden" ,
                            "margin-bottom" : "0" ,
                            "padding-bottom" : "0" ,
                            "display" : getVisibleDisplayValue( this )
                        } );
                    } ,
                    
                    done : function ( p ) {
                        if ( !p ) {
                            stylesSnapshot.recover( this );
                        }
                        
                        checkUselessProp( element , saved , 'show' );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            slideUp : function ( element , dur , call ) {
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    properties : {
                        "height" : "0" ,
                        "margin-top" : "0" ,
                        "padding-top" : "0" ,
                        "margin-bottom" : "0" ,
                        "padding-bottom" : "0"
                    } ,
                    
                    begin : function () {
                        if ( is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        sdp.set( this );
                        
                        stylesSnapshot.make( this , [
                            "height" ,
                            "overflow" ,
                            "margin-top" ,
                            "padding-top" ,
                            "margin-bottom" ,
                            "padding-bottom" ,
                            "pointer-events"
                        ] );
                        
                        setCss( this , {
                            "overflow" : "hidden" ,
                            "pointer-events" : "none" ,
                            "height" : ( this.clientHeight || this.offsetHeight ) + 'px'
                        } );
                    } ,
                    
                    done : function ( p ) {
                        checkUselessProp( element , saved , 'hide' );
                        
                        if ( !p ) {
                            this.style.display = 'none';
                            stylesSnapshot.recover( this );
                        }
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            slideRight : function ( element , dur , call ) {
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    width = calcProp( element , 'width' ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    properties : {
                        "width" : width ,
                        "margin-left" : getstyleproperty( element , "margin-left" ) ,
                        "padding-left" : getstyleproperty( element , "padding-left" ) ,
                        "margin-right" : getstyleproperty( element , "margin-right" ) ,
                        "padding-right" : getstyleproperty( element , "padding-right" )
                    } ,
                    
                    begin : function () {
                        if ( !is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        stylesSnapshot.make( this , [
                            "width" ,
                            "overflow" ,
                            "margin-left" ,
                            "padding-left" ,
                            "margin-right" ,
                            "padding-right"
                        ] );
                        
                        setCss( this , {
                            "width" : "0" ,
                            "margin-left" : "0" ,
                            "padding-left" : "0" ,
                            "margin-right" : "0" ,
                            "overflow" : "hidden" ,
                            "padding-right" : "0" ,
                            "display" : getVisibleDisplayValue( this )
                        } );
                    } ,
                    
                    done : function () {
                        checkUselessProp( element , saved , 'show' );
                        
                        stylesSnapshot.recover( this );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            slideLeft : function ( element , dur , call ) {
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    properties : {
                        "width" : "0" ,
                        "margin-left" : "0" ,
                        "padding-left" : "0" ,
                        "margin-right" : "0" ,
                        "padding-right" : "0"
                    } ,
                    
                    begin : function () {
                        if ( is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        sdp.set( this );
                        
                        stylesSnapshot.make( this , [
                            "width" ,
                            "overflow" ,
                            "margin-left" ,
                            "padding-left" ,
                            "margin-right" ,
                            "padding-right" ,
                            "pointer-events"
                        ] );
                        
                        setCss( this , {
                            "overflow" : "hidden" ,
                            "pointer-events" : "none" ,
                            "width" : ( this.clientWidth || this.offsetWidth ) + 'px'
                        } );
                    } ,
                    
                    done : function () {
                        checkUselessProp( element , saved , 'hide' );
                        
                        this.style.display = 'none';
                        stylesSnapshot.recover( this );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            toggleSlide : function ( element , dur , call ) {
                if ( is_hidden( element ) ) {
                    this.slideDown( element , dur , call );
                }
                else {
                    this.slideUp( element , dur , call );
                }
            } ,
            
            turnIn : function ( element , dur , call ) {
                // get timing and callback
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                var css = {
                    'opacity' : getRightOpacity( saved.opacity )
                };
                
                css[ _anime.prefix.transform + 'transform' ] = 'rotateY(0deg) rotateX(0deg)';
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    how : 'ease' ,
                    properties : css ,
                    
                    begin : function () {
                        if ( !is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        stylesSnapshot.make( this , [
                            "opacity" ,
                            "visibility"
                        ] );
                        
                        // before start set first property of turn
                        var css = {
                            'opacity' : '0' ,
                            'visibility' : 'visible' ,
                            'display' : getVisibleDisplayValue( this )
                        };
                        
                        css[ _anime.prefix.transform + 'transform' ] = 'rotateY(90deg) rotateX(90deg)';
                        
                        setCss( this , css );
                    } ,
                    
                    done : function () {
                        checkUselessProp( element , saved , 'show' );
                        
                        var css = {};
                        
                        css[ _anime.prefix.transform + 'transform' ] = '';
                        
                        setCss( this , css );
                        
                        stylesSnapshot.recover( this );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            turnOut : function ( element , dur , call ) {
                // get timing and callback
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                var css = {
                    'opacity' : '0'
                };
                
                css[ _anime.prefix.transform + 'transform' ] = 'rotateY(90deg) rotateX(90deg)';
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    how : 'ease' ,
                    properties : css ,
                    
                    begin : function () {
                        if ( is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        sdp.set( this );
                        
                        stylesSnapshot.make( this , [
                            "opacity" ,
                            "pointer-events"
                        ] );
                        
                        setCss( this , {
                            "pointer-events" : "none"
                        } );
                    } ,
                    
                    done : function () {
                        checkUselessProp( element , saved , 'hide' );
                        
                        var css = { 'display' : 'none' };
                        
                        css[ _anime.prefix.transform + 'transform' ] = '';
                        
                        setCss( this , css );
                        
                        stylesSnapshot.recover( this );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            toggleTurn : function ( element , dur , call ) {
                if ( is_hidden( element ) ) {
                    this.turnIn( element , dur , call );
                }
                else {
                    this.turnOut( element , dur , call );
                }
            } ,
            
            plopIn : function ( element , dur , call ) {
                // get timing and callback
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                var css = {
                    'opacity' : getRightOpacity( saved.opacity )
                };
                
                css[ _anime.prefix.transform + 'transform' ] = 'scale(1)';
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    how : 'ease' ,
                    properties : css ,
                    
                    begin : function () {
                        if ( !is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        stylesSnapshot.make( this , [
                            "opacity" ,
                            "visibility"
                        ] );
                        
                        // before start set first property of turn
                        var css = {
                            'opacity' : '0' ,
                            'visibility' : 'visible' ,
                            'display' : getVisibleDisplayValue( this )
                        };
                        
                        css[ _anime.prefix.transform + 'transform' ] = 'scale(.8)';
                        
                        setCss( this , css );
                    } ,
                    
                    done : function () {
                        checkUselessProp( element , saved , 'show' );
                        
                        var css = {};
                        
                        css[ _anime.prefix.transform + 'transform' ] = '';
                        
                        setCss( this , css );
                        
                        stylesSnapshot.recover( this );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            plopOut : function ( element , dur , call ) {
                // get timing and callback
                var saved = save( element , [ 'opacity' , 'visibility' , 'display' ] ) ,
                    data = dataAnim( dur , call ) ,
                    back = data[ 0 ];
                
                var css = {
                    'opacity' : '0'
                };
                
                css[ _anime.prefix.transform + 'transform' ] = 'scale(.8)';
                
                _anime( {
                    element : element ,
                    duration : data[ 1 ] ,
                    how : 'ease' ,
                    properties : css ,
                    
                    begin : function () {
                        if ( is_hidden( this ) ) {
                            if ( !!back ) {
                                back.call( this );
                            }
                            
                            return 'stop';
                        }
                        
                        sdp.set( this );
                        
                        stylesSnapshot.make( this , [
                            "opacity" ,
                            "pointer-events"
                        ] );
                        
                        setCss( this , {
                            "pointer-events" : "none"
                        } );
                    } ,
                    
                    done : function () {
                        checkUselessProp( element , saved , 'hide' );
                        
                        var css = { 'display' : 'none' };
                        
                        css[ _anime.prefix.transform + 'transform' ] = '';
                        
                        setCss( this , css );
                        
                        stylesSnapshot.recover( this );
                        
                        if ( !!back ) {
                            back.call( this );
                        }
                    }
                } );
            } ,
            
            togglePlop : function ( element , dur , call ) {
                if ( is_hidden( element ) ) {
                    this.plopIn( element , dur , call );
                }
                else {
                    this.plopOut( element , dur , call );
                }
            } ,
            
            show : function ( element ) {
                element.style.display = getVisibleDisplayValue( element );
            } ,
            
            hide : function ( element ) {
                sdp.set( element );
                element.style.display = 'none';
            } ,
            
            toggle : function ( element , dur , call ) {
                if ( is_hidden( element ) ) {
                    this.show( element , dur , call );
                }
                else {
                    this.hide( element , dur , call );
                }
            }
            
        };
    }
    
    /**
     * @public
     * @readonly
     * @function animationBuild
     * */
    _defineProperty( global , 'animationBuild' , function ( d , w ) {
        return build( getdocument( d ) || global.document , getwindow( w ) || global );
    } );
    
} )();