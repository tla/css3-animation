/**
 * @method $.fn.animation
 * set style property with animation
 *
 * @argument {Object|string} css - CSS property | CSS key
 * @argument {number|string|function} [ms = 400] - Duration in millisecond or "slow" / "fast" | CSS Value | callback when animation is finish
 * @argument {number|function} [opt] -  Animation duration in millisecond ( if "ms" is a string ) | callback when animation is finish
 * @argument {function} [call] -  callback when animation is finish ( if "ms" is a string and "opt" is a number )
 *
 * @return {Object} jQuery
 * */

/**
 * @method $.fn.animationWait
 * pause animation
 *
 * @argument {number|function} [delay = 400] - Duration in millisecond or "slow" / "fast"  | callback when duration is finish
 * @argument {function} [call] - callback when duration is finish
 *
 * @return {Object} jQuery
 * */

/**
 * @method $.fn.fadeIn
 * show current selector with CSS transistion
 *
 * @argument {number|function} [dur = 400] - Duration in millisecond or "slow" / "fast"  | callback when animation is finish
 * @argument {function} [call] - callback when duration is finish
 *
 * @return {Object} jQuery
 * */

/**
 * @method $.fn.fadeOut
 * hide current selector with CSS transistion
 *
 * @argument {number|function} [dur = 400] - Duration in millisecond or "slow" / "fast"  | callback when animation is finish
 * @argument {function} [call] - callback when duration is finish
 *
 * @return {Object} jQuery
 * */

/**
 * @method $.fn.slideUp
 * show current selector with CSS transistion
 *
 * @argument {number|function} [dur = 400] - Duration in millisecond or "slow" / "fast"  | callback when animation is finish
 * @argument {function} [call] - callback when duration is finish
 *
 * @return {Object} jQuery
 * */

/**
 * @method $.fn.slideLeft
 * show current selector with CSS transistion
 *
 * @argument {number|function} [dur = 400] - Duration in millisecond or "slow" / "fast"  | callback when animation is finish
 * @argument {function} [call] - callback when duration is finish
 *
 * @return {Object} jQuery
 * */

/**
 * @method $.fn.slideDown
 * hide current selector with CSS transistion
 *
 * @argument {number|function} [dur = 400] - Duration in millisecond or "slow" / "fast"  | callback when animation is finish
 * @argument {function} [call] - callback when duration is finish
 *
 * @return {Object} jQuery
 * */

/**
 * @method $.fn.slideRight
 * hide current selector with CSS transistion
 *
 * @argument {number|function} [dur = 400] - Duration in millisecond or "slow" / "fast"  | callback when animation is finish
 * @argument {function} [call] - callback when duration is finish
 *
 * @return {Object} jQuery
 * */

(function () {
    
    var animation = animationBuild() ,
        fn = [ 'animationWait' , 'fadeIn' , 'fadeOut' , 'slideUp' , 'slideDown' , 'slideLeft' , 'slideRight' ];
    
    $.fn.animation = function ( css , ms , opt , call ) {
        return $( this ).each( function () {
            animation.animate( this , css , ms , opt , call );
        } );
    };
    
    fn.forEach( function ( field ) {
        $.fn[ field ] = function ( d , c ) {
            return $( this ).each( function () {
                animation.wait( this , d , c );
            } );
        };
    } );
    
})();